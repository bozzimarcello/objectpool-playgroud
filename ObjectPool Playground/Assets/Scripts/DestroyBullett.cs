﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBullett : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ObjectPoolManager.Instance.ReturnToPool(collision.gameObject);
    }
}
