﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
    [SerializeField] public GameObject projectilePrefab;
    [SerializeField] public int poolSize;

    private GameObject[] pool;
    private int nextObject;

    private static ObjectPoolManager instance;
    public static ObjectPoolManager Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.LogError("ERROR: There are two instances of this Singleton! The first is:" + gameObject);
            Debug.LogError("ERROR: There are two instances of this Singleton! The second is:" + instance.gameObject);
        }

        InitPool();
    }

    public void InitPool ()
    {
        pool = new GameObject[poolSize];
        for (int i = 0; i< poolSize; i++)
        {
            pool[i] = Instantiate(projectilePrefab);
            pool[i].SetActive(false);
        }
        nextObject = poolSize - 1;
    }

    public GameObject GetFromPool(Vector3 pos, Quaternion rot)
    {
        GameObject newObject = pool[nextObject];
        newObject.SetActive(true);
        newObject.transform.position = pos;
        newObject.transform.rotation = rot;
        pool[nextObject] = null;
        Debug.Log("GetFromPool" + newObject.name + "" + nextObject);
        nextObject--;
        return newObject;
    }

    public void ReturnToPool(GameObject go)
    {
        go.SetActive(false);
        nextObject++;
        pool[nextObject] = go;
        Debug.Log("ReturnToPool" + go.name + "" + nextObject);
    }


}
