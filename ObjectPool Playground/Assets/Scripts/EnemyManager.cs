﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] public GameObject enemyPrefab;
    [SerializeField] public int poolSize;

    private List<GameObject> pool;

    private float xMin;
    private float xMax;
    private float yMin;
    private float yMax;
    [SerializeField] float xPadding = 2f;
    [SerializeField] float yPadding = 1f;
    [SerializeField] private float spawnRate = 1f;

    private static EnemyManager instance;

    public static EnemyManager Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.LogError("ERROR: There are two instances of this Singleton! The first is:" + gameObject);
            Debug.LogError("ERROR: There are two instances of this Singleton! The second is:" + instance.gameObject);
        }

        InitPool();
        SetupMoveBoundaries();
        StartCoroutine("SpawnAnEnemy");
    }

    public void InitPool()
    {
        pool = new List<GameObject>();
        for (int i = 0; i < poolSize; i++)
        {
            GameObject newGameObject = Instantiate(enemyPrefab);
            newGameObject.SetActive(false);
            pool.Add(newGameObject);
        }
    }

    private void SetupMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + xPadding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - xPadding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + yPadding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - yPadding;
    }

    public GameObject GetFromPool(Vector3 pos, Quaternion rot)
    {
        GameObject newObject = pool[pool.Count-1];
        newObject.SetActive(true);
        newObject.transform.position = pos;
        newObject.transform.rotation = rot;
        pool.RemoveAt(pool.Count - 1);
        return newObject;
    }

    public void ReturnToPool(GameObject go)
    {
        go.SetActive(false);
        pool.Add(go);
    }

    IEnumerator SpawnAnEnemy()
    {
        while(true)
        {
            Debug.Log("spawn");
            Vector3 position = new Vector3(UnityEngine.Random.Range(xMin, xMax), UnityEngine.Random.Range(yMin, yMax), 0);
            GetFromPool(position, Quaternion.identity);
            yield return new WaitForSeconds(spawnRate);
        }
    }
}
